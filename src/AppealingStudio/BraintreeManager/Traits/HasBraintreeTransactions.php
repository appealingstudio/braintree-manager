<?php

namespace AppealingStudio\BraintreeManager\Traits;

use AppealingStudio\BraintreeManager\Models\BraintreeTransaction;

/**
 * Extend models that will have Braintree transactions
 */
trait HasBraintreeTransactions
{
	/**
	 * Get the wepay activity bound to the transaction
	 *
	 * @return BraintreeTransaction
	 */
	public function braintree()
	{
		return $this->hasOne('AppealingStudio\BraintreeManager\Models\BraintreeTransaction');
	}
}
